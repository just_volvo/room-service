<?php

namespace app\controllers;

use Yii;
use app\models\search\RoomSearch;
use app\models\Reservation;
use app\models\search\ReservationSearch;
use app\components\ReservationFailedException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ReservationController implements the CRUD actions for Reservation model.
 */
class ReservationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['reservationIndex']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['deactivate'],
                        'roles' => ['reservationDeactivate'],
                        'roleParams' => function() {
                            return ['reservation' => Reservation::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['reservationView'],
                        'roleParams' => function() {
                            return ['reservation' => Reservation::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['my-reservations'],
                        'roles' => ['reservationMyReservations']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['find-room'],
                        'roles' => ['reservationFindRoom']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['reserve'],
                        'roles' => ['reservationReserve']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['reservationDelete']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'reserve' => ['POST'],
                    'deactivate' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reservation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->title = Yii::t('app', 'All reservations');
        $searchModel = new ReservationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDeactivate($id)
    {
        $reservation = $this->findModel($id);
        $status = ($reservation->user_id == Yii::$app->user->id) ? $reservation::STATUS_INACTIVE : $reservation::STATUS_CANCELED_BY_ADMIN;
        $reservation->updateAttributes(['status' => $status]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return string
     */
    public function actionMyReservations()
    {
        $this->view->title = Yii::t('app', 'My reservations');

        $userId = Yii::$app->user->id;
        $searchModel = new ReservationSearch(['user_id' => $userId]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        }

        return $this->render('view', [
                'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionFindRoom()
    {
        $searchModel = new RoomSearch();
        $dataProvider = $searchModel->findRoom(Yii::$app->request->queryParams);

        return $this->render('find-room', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionReserve($id)
    {
        $searchModel = new RoomSearch();
        $searchModel->load(Yii::$app->request->bodyParams);

        if (!$searchModel->validate()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($searchModel);
        }

        $reservation = new Reservation([
            'room_id' => $id,
            'user_id' => Yii::$app->user->id,
            'meeting_date' => Yii::$app->formatter->asDate($searchModel->meetingDate, 'yyyy-MM-dd'),
            'start_time' => $searchModel->startTime,
            'end_time' => $searchModel->endTime,
        ]);

        try {
            $reservation->makeReservation();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Selected room is successfully reserved'));
            return $this->redirect(['/reservation/my-reservations']);
        } catch (ReservationFailedException $exception) {
            Yii::$app->session->setFlash('error', $exception->getMessage());
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Reservation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reservation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reservation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reservation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
