<?php

use app\rbac\rules\AuthorRule;
use dektrium\rbac\migrations\Migration;

class m170626_040732_rbacInit extends Migration
{
    public function safeUp()
    {
        $administrator = $this->createRole('administrator', Yii::t('app/users', 'Administrator'));
        $user = $this->createRole('user', Yii::t('app/users', 'User'));

        $this->createPermission('siteIndex', Yii::t('app/users', 'Site index page'));

        $this->createPermission('roomIndex', Yii::t('app/users', 'Room index page'));
        $this->createPermission('roomCreate', Yii::t('app/users', 'Create Room'));
        $this->createPermission('roomUpdate', Yii::t('app/users', 'Update Room'));
        $this->createPermission('roomDelete', Yii::t('app/users', 'Delete Room'));

        $roomManager = $this->createRole('roomManage', 'Room manage');
        $this->addChild($roomManager, 'roomIndex');
        $this->addChild($roomManager, 'roomCreate');
        $this->addChild($roomManager, 'roomUpdate');
        $this->addChild($roomManager, 'roomDelete');

        $this->addChild($user, 'siteIndex');
        $this->addChild($administrator, $roomManager);

        $this->createPermission('reservationIndex', Yii::t('app/users', 'Reservation index page'));
        $this->createPermission('reservationView', Yii::t('app/users', 'View reservation'));
        $this->createPermission('reservationDelete', Yii::t('app/users', 'Delete reservation'));
        $this->createPermission('reservationReserve', Yii::t('app/users', 'Create reservation'));
        $this->createPermission('reservationDeactivate', Yii::t('app/users', 'Deactivate reservation'));
        $this->createPermission('reservationMyReservations', Yii::t('app/users', 'View own reservation'));
        $this->createPermission('reservationFindRoom', Yii::t('app/users', 'Find room'));

        $authorRule = new AuthorRule();
        $authorRule = $this->createRule($authorRule->name, $authorRule::className());
        $this->createPermission('reservationDeactivateOwn', Yii::t('app/users', 'Deactivate reservation'), $authorRule->name);
        $this->createPermission('reservationViewOwn', Yii::t('app/users', 'View owned reservation'), $authorRule->name);
        $this->addChild('reservationViewOwn', 'reservationView');
        $this->addChild('reservationDeactivateOwn', 'reservationDeactivate');

        $this->addChild($user, 'reservationViewOwn');
        $this->addChild($user, 'reservationDeactivateOwn');
        $this->addChild($user, 'reservationReserve');
        $this->addChild($user, 'reservationMyReservations');
        $this->addChild($user, 'reservationFindRoom');

        $reservationManager = $this->createRole('reservationManage', 'Reservation manage');
        $this->addChild($reservationManager, 'reservationIndex');
        $this->addChild($reservationManager, 'reservationView');
        $this->addChild($reservationManager, 'reservationDelete');
        $this->addChild($reservationManager, 'reservationReserve');
        $this->addChild($reservationManager, 'reservationDeactivate');
        $this->addChild($reservationManager, 'reservationMyReservations');
        $this->addChild($reservationManager, 'reservationFindRoom');

        $this->addChild($administrator, $reservationManager);
        $this->addChild($administrator, $user);
    }
    
    public function safeDown()
    {
        $this->authManager->removeAll();
    }
}
