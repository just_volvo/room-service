<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
$this->context->layout = '@app/views/layouts/empty';
?>
<div class="middle-box text-center">
    <h1><?= $exception->statusCode; ?></h1>
    <h3 class="font-bold"><?= nl2br(Html::encode($message)) ?></h3>

    <div class="error-desc">
        <?= Yii::t('app', 'The above error occurred while the Web server was processing your request.')?>
        <?= Yii::t('app', 'Please contact us if you think this is a server error. Thank you.')?>
        <div class="m-t">
            <?= Html::button(Yii::t('app', 'Previous page'), ['class' => 'btn btn-primary', 'onclick' => 'history.back();']) ?>
            <?= Html::a(Yii::t('app', 'Home'), Url::home(), ['class' => 'btn btn-success']) ?>
        </div>

    </div>
</div>
