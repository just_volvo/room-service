<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Room reservation service');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::t('app', 'Welcome!')?></h1>

        <p class="lead"><?= Yii::t('app', 'Some service description') ?></p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com"><?= Yii::t('app', 'Find room') ?></a></p>
    </div>

    <div class="body-content">

    </div>
</div>
