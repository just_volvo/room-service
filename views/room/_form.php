<?php

use app\models\RoomFeature;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'capacity')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'featureIds')->checkboxList(RoomFeature::getCheckboxList(),
        [
            'item' => function ($index, $label, $name, $checked, $value) {
                $checkedAttribute = ($checked) ? 'checked' : '';
                return "<input id='feature-{$index}' class='switcher' type='checkbox' {$checkedAttribute}  name='{$name}' value='{$value}'>"
                    . "<label for='feature-{$index}'>{$label}</label>";
            },
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
