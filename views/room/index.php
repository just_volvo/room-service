<?php

use app\models\Room;
use app\models\RoomFeature;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rooms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Room'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style' => 'width: 50px']
            ],
            'name',
            'capacity',
            [
                'attribute' => 'featureIds',
                'filter' => Html::activeDropDownList($searchModel, 'featureSearch',
                    RoomFeature::getCheckboxList(), ['prompt' => Yii::t('app', 'Select room features'), 'class' => 'form-control']),
                'content' => function ($data) {
                    /** @var Room $data */
                    return $data->featuresAsHtml();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width: 80px;'],
                'visibleButtons' => [
                    'view' => true,
                    'update' => true,
                    'delete' => true,
                ],
            ],
        ],
    ]); ?>
</div>
