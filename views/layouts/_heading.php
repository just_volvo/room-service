<?php
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
?>

<h2><?= $this->title ?></h2>

<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n"
]) ?>

