<?php
use app\widgets\Menu;

?>

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <?php
        $header = [
            'name' => Yii::$app->user->identity->username,
            'email' => Yii::$app->user->identity->email,
            'url' => ['/user/profile/index'],
        ];

        $menu = [
            [
                'label' => Yii::t('app', 'Home'),
                'url' => ['/site/index'],
                'icon' => 'fa fa-home',
            ],
            [
                'label' => Yii::t('app', 'Profile'),
                'url' => ['#'],
                'icon' => 'fa fa-user',
                'active' => $this->context->id === 'settings',
                'items' => [
                    [
                        'label' => Yii::t('user', 'Account settings'),
                        'url' => ['/user/settings/account'],
                    ],
                    [
                        'label' => Yii::t('user', 'Profile settings'),
                        'url' => ['/user/settings/profile'],
                    ],
                    [
                        'label' => Yii::t('user', 'Networks'),
                        'url' => ['/user/settings/networks'],
                    ],
                ],
            ],
            [
                'label' => Yii::t('app', 'Rooms'),
                'url' => ['/room/index'],
                'icon' => 'fa fa-pie-chart',
                'active' => $this->context->id === 'room',
                'visible' => Yii::$app->user->can('roomManage'),
            ],
            [
                'label' => Yii::t('app', 'Reservations'),
                'url' => ['#'],
                'icon' => 'fa fa-star',
                'active' => $this->context->id === 'reservation',
                'items' => [
                    [
                        'label' => Yii::t('app', 'All reservations'),
                        'url' => ['/reservation/index'],
                        'visible' => Yii::$app->user->can('reservationIndex'),
                    ],
                    [
                        'label' => Yii::t('app', 'My reservations'),
                        'url' => ['/reservation/my-reservations'],
                        'visible' => Yii::$app->user->can('reservationMyReservations'),
                    ],
                    [
                        'label' => Yii::t('app', 'Find room'),
                        'url' => ['/reservation/find-room'],
                        'visible' => Yii::$app->user->can('reservationFindRoom'),
                    ],
                ],
                'visible' => Yii::$app->user->can('user'),
            ],
            [
                'label' => Yii::t('app', 'Users'),
                'url' => ['/user/admin/index'],
                'icon' => 'fa fa-users',
                'visible' => Yii::$app->user->can('administrator'),
            ],
        ];
        ?>

        <?= Menu::widget([
            'options' => ['class' => 'nav metismenu', 'id' => 'side-menu'],
            'submenuTemplate' => "\n<ul class='nav nav-second-level collapse' {show}>\n{items}\n</ul>\n",
            'items' => $menu,
            'header' => $header,
        ]) ?>

    </div>
</nav>