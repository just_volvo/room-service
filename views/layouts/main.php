<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppFooterAsset;
use app\assets\AppHeaderAsset;
use yii\helpers\Html;
use app\widgets\Alert;


AppHeaderAsset::register($this);
AppFooterAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<div id="wrapper">

    <?= $this->render('_sidebar') ?>

    <div id="page-wrapper" class="gray-bg">

        <section class="row border-bottom">
            <?= $this->render('_header') ?>
        </section>

        <section class="row border-bottom white-bg page-heading">
            <div class="col-sm-12">
                <?= $this->render('_heading') ?>
            </div>
        </section>

        <main class="wrapper wrapper-content">
            <?= Alert::widget() ?>
            <section><?= $content ?></section>
        </main>

    </div>

</div>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
