<?php
use yii\helpers\Url;

?>

<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <a href="<?= Url::toRoute(['/user/security/logout']) ?>" data-method="post">
                <i class="fa fa-sign-out"></i> Выйти
            </a>
        </li>
    </ul>
</nav>