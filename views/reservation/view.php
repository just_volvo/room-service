<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reservation */

$this->title = Yii::t('app', 'Meeting Date') . ': ' . $model->meeting_date;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'View reservation'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'user.username',
                'visible' => Yii::$app->user->can('administrator'),
            ],
            'start_time',
            'end_time',
            'room.name',
            'room.capacity',
            [
                'attribute' => 'room.featureIds',
                'format' => 'raw',
                'value' => $model->room->featuresAsHtml(),
            ],
        ],
    ]) ?>

</div>
