<?php

use app\models\RoomFeature;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\RoomSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservation-search">

    <?php $form = ActiveForm::begin([
        'id' => 'room-search-form',
        'action' => ['find-room'],
        'method' => 'get',
    ]); ?>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <h3><?= Yii::t('app', 'Select room features') ?></h3>

            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'name') ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'capacity') ?>
                </div>
            </div>

            <?= $form->field($model, 'featureIds')->checkboxList(RoomFeature::getCheckboxList(), [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $checkedAttribute = ($checked) ? 'checked' : '';
                    return "<input id='feature-{$index}' class='switcher' type='checkbox' {$checkedAttribute}  name='{$name}' value='{$value}'>"
                        . "<label for='feature-{$index}'>{$label}</label>";
                },
            ]) ?>

            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'availableOnly', [
                        'template' => '{input}{label}{error}{hint}',
                    ])->checkbox(['class' => 'switcher'], false) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <h3><?= Yii::t('app', 'Enter the meeting time') ?></h3>

            <div class="row">
                <?php $timePickerOptions = [
                    'pluginOptions' => [
                        'minuteStep' => 15,
                        'showMeridian' => false,
                    ],
                ]; ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'meetingDate')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => Yii::t('app', 'Select meeting date')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'startTime')->widget(TimePicker::classname(), $timePickerOptions); ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'endTime')->widget(TimePicker::classname(), $timePickerOptions); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
