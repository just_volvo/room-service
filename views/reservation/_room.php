<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Room */

?>
<div class="col-lg-4 col-md-6">
    <h2 class="text-center"><?= Html::encode($model->name) ?></h2>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'capacity',
            [
                'attribute' => 'featureIds',
                'format' => 'raw',
                'value' => $model->featuresAsHtml(),
            ],
        ],
    ]) ?>

    <div class="text-center">
        <?= Html::a(Yii::t('app', 'Reserve'), ['/reservation/reserve', 'id' => $model->id], [
            'class' => 'btn btn-success room-reserve',
            'data' => [
                'source' => '#room-search-form',
            ],
        ]) ?>
    </div>
</div>

