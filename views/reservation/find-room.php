<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ReservationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Find room');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-index">
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_room',
        'layout' => '{summary} <div class="row">{items}</div> {pager}',
    ]); ?>
</div>
