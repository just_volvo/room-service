<?php

use app\models\Reservation;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ReservationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style' => 'width: 50px']
            ],
            [
                'attribute' => 'room',
                'value' => 'room.name',
            ],
            [
                'attribute' => 'user',
                'value' => 'user.username',
                'visible' => Yii::$app->user->can('administrator'),
            ],
            [
                'attribute' => 'meeting_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'type' => DatePicker::TYPE_RANGE,
                    'attribute' => 'meetingDateFrom',
                    'attribute2' => 'meetingDateTo',
                    'separator' => '',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ],
                ]),
            ],
            [
                'attribute' => 'start_time',
                'filter' => \yii\widgets\MaskedInput::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_time',
                    'mask' => '99:99',
                ]),
            ],
            [
                'attribute' => 'end_time',
                'filter' => \yii\widgets\MaskedInput::widget([
                    'model' => $searchModel,
                    'attribute' => 'end_time',
                    'mask' => '99:99',
                ]),
            ],
            [
                'attribute' => 'status',
                'filter' => Reservation::statusArray(),
                'content' => function ($data) {
                    return Reservation::statusArrayHtml()[$data->status];
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width: 60px;'],
                'template' => '{view} {deactivate}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-eye-open"]);
                        return Html::a($icon, ['/reservation/view', 'id' => $key],
                            [
                                'title' => Yii::t('yii', 'View'),
                                'data' => [
                                    'toggle' => 'modal',
                                    'target' => '#reservation-view-modal',
                                    'pjax' => 0,
                                ],
                            ]);
                    },
                    'deactivate' => function ($url, $model, $key) {
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-remove"]);
                        return Html::a($icon, ['/reservation/deactivate', 'id' => $key],
                            [
                                'title' => Yii::t('app', 'Cancel'),
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => Yii::t('app', 'Are you sure you want to deactivate this reservation?')
                                ],
                            ]);
                    }
                ],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                             return Yii::$app->user->can('reservationView', ['reservation' => $model]);
                         },
                    'deactivate' => function ($model, $key, $index) {
                        return $model->status == Reservation::STATUS_ACTIVE;
                    },
                ],
            ],
        ],
    ]); ?>
</div>

<?= Modal::widget([
    'id' => 'reservation-view-modal',
    'header' => Yii::t('app', 'View reservation'),
    'options' => ['class' => 'ajax-modal'],
    'toggleButton' => false,
]); ?>