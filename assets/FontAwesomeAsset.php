<?php

namespace app\assets;

/**
 * Class FontAwesomeAsset
 */
class FontAwesomeAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/fortawesome/font-awesome';
    public $css = ['css/font-awesome.min.css'];
}