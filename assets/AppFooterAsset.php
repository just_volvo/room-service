<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Application Footer asset bundle.
 */
class AppFooterAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/theme.js',
        'js/site.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/pace/pace.min.js',
    ];
    public $jsOptions = ['position' => View::POS_END];
}
