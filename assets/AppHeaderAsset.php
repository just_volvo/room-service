<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Application header asset bundle.
 */
class AppHeaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/theme.css',
        'css/site.css',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesomeAsset',
    ];
}
