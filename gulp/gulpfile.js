var gulp = require('gulp'),
    rename = require('gulp-rename'),
    /*Css-минимизация*/
    autoprefixer = require('gulp-autoprefixer'),
    cssMin = require('gulp-css'),
    stripCssComments = require('gulp-strip-css-comments'),
    /*Css-минимизация END*/
    /*Js-минимизация*/
    uglify = require('gulp-uglify'),
    /*Js-минимизация END*/
    /* Для командной строки Yii*/ 
    minimist = require('minimist'),
    options = minimist(process.argv.slice(2), {string: ['src', 'dist']});
    /* Для командной строки Yii END*/ 

gulp.task('css_compress', function () {
    var destDir = options.dist.substring(0, options.dist.lastIndexOf("/")),
        destFile = options.dist.replace(/^.*[\\\/]/, '');
    return gulp.src(options.src)
        .pipe(stripCssComments())
        .pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
        .pipe(cssMin())
        .pipe(rename(destFile))
        .pipe(gulp.dest(destDir));
});

gulp.task('scripts_compress', function () {
    var destDir = options.dist.substring(0, options.dist.lastIndexOf("/")),
        destFile = options.dist.replace(/^.*[\\\/]/, '');
    return gulp.src(options.src)
        .pipe(uglify())
        .pipe(rename(destFile))
        .pipe(gulp.dest(destDir));
});
