<?php
/**
 * Configuration file for the "yii asset" console command.
 * Yii command:
 * yii asset gulp/assets-config.php config/assets-prod.php
 */
use yii\web\View;

Yii::setAlias('@webroot', __DIR__ . '/../web');
Yii::setAlias('@web', '/');

return [
    'jsCompressor' => 'gulp scripts_compress --gulpfile gulp/gulpfile.js --src {from} --dist {to}',
    'cssCompressor' => 'gulp css_compress --gulpfile gulp/gulpfile.js --src {from} --dist {to}',
    // Удалять исходные файлы после сжатия:
    'deleteSource' => false,
    // Список подключаемых ресурсов:
    'bundles' => [
        'app\assets\AppHeaderAsset',
        'app\assets\AppFooterAsset',
        //'yii\widgets\MaskedInputAsset',
        //'yii\widgets\ActiveFormAsset',
        //'yii\validators\ValidationAsset',
    ],
    'targets' => [
        //Все наши скрипты подключаем в футере
        'all-footer' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/dist',
            'baseUrl' => '@web/dist',
            'js' => 'js/all-footer-{hash}.js',
            'jsOptions' => ['position' => View::POS_END],
            'depends' => [
                'app\assets\AppFooterAsset',
            ],
        ],
        //Все остальные скрипты и css подключаем в шапке (jQuery, bootstrap и т.п.)
        'all-header' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/dist',
            'baseUrl' => '@web/dist',
            'js' => 'js/all-{hash}.js',
            'css' => 'css/all-{hash}.css',
            'depends' => [
            ],
        ],
    ],
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];