<?php

use yii\db\Migration;

class m170625_092603_addStatusToReservationTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%reservation}}', 'status', $this->smallInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%reservation}}', 'status');
    }
}
