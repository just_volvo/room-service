<?php

use yii\db\Migration;

class m170627_062334_createDefaultAdmin extends Migration
{
    public function safeUp()
    {
        $user = new \dektrium\user\models\User([
            'username' => 'administrator@administrator.com',
            'email' => 'administrator@administrator.com',
            'password' => 'administrator@administrator.com',
        ]);
        $user->create();

        $auth = Yii::$app->authManager;
        $role = $auth->getRole('administrator');
        $auth->assign($role, $user->id);
    }

    public function safeDown()
    {
        return true;
    }
}
