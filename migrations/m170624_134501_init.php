<?php

use yii\db\Migration;

class m170624_134501_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%room}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'capacity' => $this->smallInteger(),
        ], $tableOptions);

        $this->createTable('{{%room_feature}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
        ], $tableOptions);

        $this->batchInsert('{{%room_feature}}',
            ['name'],
            [
                ['Проектор'],
                ['Маркерная доска'],
                ['Конференцсвязь'],
            ]);

        $this->createTable('{{%room_feature_item}}', [
            'id' => $this->primaryKey(),
            'room_id' => $this->integer()->notNull(),
            'feature_id' => $this->integer()->notNull(),
        ], $tableOptions);
        //Создание индексов, т.к. не все движки автоматически создают индексы на поля для внешних ключей
        $this->createIndex('room_feature_item-room_id-idx', '{{%room_feature_item}}', 'room_id');
        $this->createIndex('room_feature_item-feature_id-idx', '{{%room_feature_item}}', 'feature_id');
        $this->addForeignKey('room_feature_item-room_id-fk', '{{%room_feature_item}}', 'room_id', '{{%room}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('room_feature_item-feature_id-fk', '{{%room_feature_item}}', 'feature_id', '{{%room_feature}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%reservation}}', [
            'id' => $this->primaryKey(),
            'room_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'meeting_date' => $this->date(),
            'start_time' => $this->time(),
            'end_time' => $this->time(),
        ], $tableOptions);
        $this->createIndex('reservation-room_id-idx', '{{%reservation}}', 'room_id');
        $this->createIndex('reservation-user_id-idx', '{{%reservation}}', 'user_id');
        $this->addForeignKey('reservation-room_id-fk', '{{%reservation}}', 'room_id', '{{%room}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('reservation-user_id-fk', '{{%reservation}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('room_feature_item-room_id-fk', '{{%room_feature_item}}');
        $this->dropForeignKey('room_feature_item-feature_id-fk', '{{%room_feature_item}}');
        $this->dropForeignKey('reservation-room_id-fk', '{{%reservation}}');
        $this->dropForeignKey('reservation-user_id-fk', '{{%reservation}}');

        $this->dropTable('{{%room_feature_item}}');
        $this->dropTable('{{%room_feature}}');
        $this->dropTable('{{%room}}');
        $this->dropTable('{{%reservation}}');
    }
}
