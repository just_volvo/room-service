<?php
$iniConfig = parse_ini_file(__DIR__ . '/config.ini', true);

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$baseUrl = str_replace('/web', '', (new \yii\web\Request)->getBaseUrl());

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'layout' => '@app/views/layouts/empty',
            'adminPermission' => 'administrator',
            'modelMap' => [
                'User' => [
                    'class' => '\dektrium\user\models\User',
                    'on afterCreate' => function ($event) {
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('user');
                        $auth->assign($role, $event->sender->id);
                    },
                    'on afterRegister' => function ($event) {
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole('user');
                        $auth->assign($role, $event->sender->id);
                    },
                ],
            ],
            'controllerMap' => [
                'admin' => [
                    'class'  => 'dektrium\user\controllers\AdminController',
                    'layout' => '@app/views/layouts/main',
                ],
                'settings' => [
                    'class'  => 'dektrium\user\controllers\SettingsController',
                    'layout' => '@app/views/layouts/main',
                ],
                'profile' => [
                    'class'  => 'dektrium\user\controllers\ProfileController',
                    'layout' => '@app/views/layouts/main',
                ],
            ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
            'adminPermission' => 'administrator',
        ],
    ],
    'components' => [
        'assetManager' => [
            'bundles' => YII_DEBUG ? [] : require(__DIR__ . '/' . 'assets-prod.php'),
            'appendTimestamp' => true,
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',
            'loginUrl' => ['/login'],
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'cache' => 'cache',
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => $iniConfig['oauth_google_client_id'],
                    'clientSecret' => $iniConfig['oauth_google_secret'],
                ],
            ],
        ],
        //yii2-user module view overriding
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'request' => [
            'baseUrl' => $baseUrl,
            'cookieValidationKey' => '0f43iyJ2bvxoYQcJ83mww8zm28bNZw7p',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/users' => 'user.php'
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/login' => '/user/security/login',
            ],
        ],
    ],
    'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'ruleConfig' => [
            'class' => 'dektrium\user\filters\AccessRule',
        ],
        'rules' => [
            [
                'actions' => ['login', 'register', 'connect', 'confirm', 'auth', 'resend', 'request', 'reset'],
                'allow' => true,
                'roles' => ['?'],
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
            [
                'actions' => ['error'],
                'allow' => true,
            ]
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
