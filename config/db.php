<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=rooms',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'schemaCache' => 'cache',
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 3600,
];
