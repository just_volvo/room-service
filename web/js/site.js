$(document).ready(function () {
    function loadModal(modal, block) {
        $('.modal-body', modal).html('<div class="loader"></div>');
        $('.modal-body', modal).load(block.attr('href'));
    }

    function clearModal(modal) {
        $('.modal-body', modal).html('');
    }

    $('.ajax-modal')
        .on('shown.bs.modal', function(event) {
            loadModal($(this), $(event.relatedTarget));
        })
        .on('hidden.bs.modal', function() {
            clearModal($(this));
        });



    $('.bootstrap-timepicker input').on('click', function() {
        $(this).timepicker('showWidget');
    });

    $('.room-reserve').on('click', function(){
        var form = $($(this).data('source'));
        $.ajax({
            url:  $(this).attr('href'),
            method: 'POST',
            data: form.serialize() + '&' + $.param({_csrf: yii.getCsrfToken()}),
            success: function(response) {
                form.yiiActiveForm('updateMessages', response);
            }
        });
        return false;
    });
});