<?php

namespace app\components;

use yii\validators\Validator;

/**
 * Class TimeRatioValidator
 * @package app\components
 */
class TimeRatioValidator extends Validator
{
    public $ratio;

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (strtotime($model->$attribute) % ($this->ratio * 60) != 0) {
            $model->addError($attribute, \Yii::t('app', 'Minutes must be divisible by {ratio}', [
                'ratio' => $this->ratio
            ]));
        }
    }
}