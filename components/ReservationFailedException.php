<?php

namespace app\components;

use yii\base\Exception;

/**
 * Class ReservationFailedException
 * @package app\components
 */
class ReservationFailedException extends Exception
{

}