<?php

namespace app\components;

use yii\validators\Validator;

/**
 * Class EndTimeValidator
 * @package app\components
 */
class EndTimeValidator extends Validator
{
    public $startAttribute;
    public $interval;

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (strtotime($model->$attribute) < strtotime($model->{$this->startAttribute})) {
            $model->addError($attribute, \Yii::t('app', 'End time must be greater then start time'));
        } else if ((strtotime($model->$attribute) - strtotime($model->{$this->startAttribute})) < $this->interval * 60) {
            $model->addError($attribute, \Yii::t('app', 'The minimum reservation time is {interval} minute', [
                'interval' => $this->interval,
            ]));
        };
    }
}