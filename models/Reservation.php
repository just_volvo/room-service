<?php

namespace app\models;

use app\components\EndTimeValidator;
use app\components\ReservationFailedException;
use app\components\TimeRatioValidator;
use dektrium\user\models\User;
use Yii;
use yii\bootstrap\Html;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Expression;

/**
 * This is the model class for table "{{%reservation}}".
 *
 * @property int $id
 * @property int $room_id
 * @property int $user_id
 * @property string $meeting_date
 * @property string $start_time
 * @property string $end_time
 * @property int $status
 *
 * @property Room $room
 * @property User $user
 */
class Reservation extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_CANCELED_BY_ADMIN = 2;
    const RESERVATION_TIME_RESTRICTION = 30;
    const TIME_RATIO = 15;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reservation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'user_id'], 'required'],
            [['room_id', 'user_id', 'status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_CANCELED_BY_ADMIN]],
            [['meeting_date'], 'date', 'format' => 'php:Y-m-d'],
            [['start_time', 'end_time'], 'time', 'format' => 'php:H:i'],
            [['start_time', 'end_time'], TimeRatioValidator::className(), 'ratio' => self::TIME_RATIO],
            ['end_time', EndTimeValidator::className(), 'startAttribute' => 'start_time', 'interval' => self::RESERVATION_TIME_RESTRICTION],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'room_id' => Yii::t('app', 'Room'),
            'user_id' => Yii::t('app', 'User'),
            'meeting_date' => Yii::t('app', 'Meeting Date'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'status' => Yii::t('app', 'Reservation status'),
            'room' => Yii::t('app', 'Room'),
            'user' => Yii::t('app', 'User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array Status array.
     */
    public static function statusArray()
    {
        return [
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_CANCELED_BY_ADMIN => Yii::t('app', 'Canceled by admin'),
        ];
    }

    /**
     * @return array Status array in html.
     */
    public static function statusArrayHtml()
    {
        return [
            self::STATUS_INACTIVE => Html::tag('span', Yii::t('app', 'Inactive'), ['class' => 'label label-default']),
            self::STATUS_ACTIVE => Html::tag('span', Yii::t('app', 'Active'), ['class' => 'label label-success']),
            self::STATUS_CANCELED_BY_ADMIN => Html::tag('span', Yii::t('app', 'Canceled by admin'), ['class' => 'label label-danger']),
        ];
    }

    /**
     * @param $condition
     * @return ActiveQuery
     */
    public function getExistenceCheckingQuery($condition)
    {
        return self::find()->select([new Expression('1')])->where($condition)
            ->andWhere(['status' => Reservation::STATUS_ACTIVE])
            ->andWhere(['meeting_date' => Yii::$app->formatter->asDate($this->meeting_date, 'yyyy-MM-dd')])
            ->andWhere(['or',
                ['and', ['<=', 'start_time', $this->start_time], ['>', 'end_time', $this->start_time]],
                ['and', ['<', 'start_time', $this->end_time], ['>=', 'end_time', $this->end_time]]
            ]);
    }

    /**
     * @return bool
     * @throws ReservationFailedException
     */
    public function makeReservation()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $sql = 'SELECT * FROM ' . Room::tableName() . ' WHERE id = :id FOR UPDATE';
        try {
            //Prevent racing state
            $room = Room::findBySql($sql, [':id' => $this->room_id])->one();

            $existedReservations = $this->getExistenceCheckingQuery(['room_id' => $this->room_id]);

            if (!$existedReservations->exists()) {
                if ($this->save()) {
                    $transaction->commit();
                    return true;
                } else {
                    $message = Yii::t('app', 'An error occurred during saving your reservation');
                }
            } else {
                $message = Yii::t('app', 'This room already reserved by another person');
            }
            $transaction->rollBack();
            throw new ReservationFailedException($message);
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw new ReservationFailedException(Yii::t('app', 'An error occurred during saving your reservation'));
        }
    }
}
