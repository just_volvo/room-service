<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reservation;
use yii\db\ActiveQuery;

/**
 * ReservationSearch represents the model behind the search form of `app\models\Reservation`.
 */
class ReservationSearch extends Reservation
{
    public $room;
    public $user;
    public $meetingDateFrom;
    public $meetingDateTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'room_id', 'user_id', 'status'], 'integer'],
            [['meetingDateFrom', 'meetingDateTo'], 'filter', 'filter' => function ($value) {
                return (empty($value)) ? '' : Yii::$app->formatter->asDate($value, 'yyyy-MM-dd');
            }],
            [['meeting_date', 'start_time', 'end_time', 'user', 'room', 'meetingDateFrom', 'meetingDateTo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reservation::find()->alias('res');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['user', 'room']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['user'] = [
            'asc' => ['user.name' => SORT_ASC],
            'desc' => ['user.name' => SORT_DESC],
            ];

        $dataProvider->sort->attributes['room'] = [
            'asc' => ['room.name' => SORT_ASC],
            'desc' => ['room.name' => SORT_DESC],
        ];

        $dataProvider->sort->defaultOrder = [
            'meeting_date' => SORT_DESC
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'res.id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'user.username', $this->user])
            ->andFilterWhere(['like', 'room.name', $this->room])
            ->andFilterWhere(['>=', 'meeting_date', $this->meetingDateFrom])
            ->andFilterWhere(['<=', 'meeting_date', $this->meetingDateTo])
            ->andFilterWhere(['>=', 'start_time', $this->start_time])
            ->andFilterWhere(['<=', 'end_time', $this->end_time]);

        return $dataProvider;
    }
}
