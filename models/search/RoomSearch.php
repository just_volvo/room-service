<?php

namespace app\models\search;

use app\components\EndTimeValidator;
use app\components\TimeRatioValidator;
use app\models\Reservation;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Room;
use yii\helpers\ArrayHelper;

/**
 * RoomSearch represents the model behind the search form of `app\models\Room`.
 */
class RoomSearch extends Room
{
    public $featureSearch;

    public $meetingDate;
    public $startTime;
    public $endTime;
    public $availableOnly;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'capacity', 'availableOnly'], 'integer'],
            [['startTime', 'endTime', 'meetingDate'], 'required', 'when' => function ($model) {
                return $model->availableOnly;
            }],
            [['meetingDate'], 'date', 'format' => 'php:d.m.Y'],
            [['startTime', 'endTime'], 'time', 'format' => 'php:H:i'],
            [['startTime', 'endTime'], TimeRatioValidator::className(), 'ratio' => Reservation::TIME_RATIO],
            ['endTime', EndTimeValidator::className(), 'startAttribute' => 'startTime', 'interval' => Reservation::RESERVATION_TIME_RESTRICTION],
            [['name', 'featureIds', 'featureSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'meetingDate' => Yii::t('app', 'Meeting Date'),
            'startTime' => Yii::t('app', 'Start Time'),
            'endTime' => Yii::t('app', 'End Time'),
            'availableOnly' => Yii::t('app', 'Available only'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Room::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'capacity' => $this->capacity,
        ]);

        if (!empty($this->featureSearch)) {
            $query->innerJoinWith(["roomFeatureItems as features"]);
            $query->andFilterWhere(['features.feature_id' => $this->featureSearch]);
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function findRoom($params)
    {
        $query = Room::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate() || empty($params)) {
            $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->featureIds)) {
            $query->innerJoinWith(["roomFeatureItems as features"]);
            $query->andWhere(['features.feature_id' => $this->featureIds]);
            $query->having('COUNT(DISTINCT features.feature_id) = :featuresCount', [
                ':featuresCount' => count($this->featureIds),
            ]);
        }

        if ($this->availableOnly) {
            $reservation = new Reservation([
                'meeting_date' => $this->meetingDate,
                'start_time' => $this->startTime,
                'end_time' => $this->endTime,
            ]);
            $subQuery = $reservation->getExistenceCheckingQuery('room_id = room.id');
            $query->andWhere(['not exists', $subQuery]);
        }

        $query->andFilterWhere(['>=', 'capacity', $this->capacity])
            ->andFilterWhere(['like', 'name', $this->name]);

        $query->distinct();

        return $dataProvider;
    }
}
