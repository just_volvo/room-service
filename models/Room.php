<?php

namespace app\models;

use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%room}}".
 *
 * @property int $id
 * @property string $name
 * @property int $capacity
 * @property array $featureIds
 *
 * @property Reservation[] $reservations
 * @property RoomFeatureItem[] $roomFeatureItems
 * @property RoomFeature[] $features
 */
class Room extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%room}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'capacity'], 'required'],
            ['name', 'unique'],
            [['capacity'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['featureIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'capacity' => Yii::t('app', 'Capacity'),
            'featureIds' => Yii::t('app', 'Features'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservations()
    {
        return $this->hasMany(Reservation::className(), ['room_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomFeatureItems()
    {
        return $this->hasMany(RoomFeatureItem::className(), ['room_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatures()
    {
        return $this->hasMany(RoomFeature::className(), ['id' => 'feature_id'])->via('roomFeatureItems');
    }

    private $_featureIds = null;

    /**
     * @return array|null
     */
    public function getFeatureIds()
    {
        if ($this->_featureIds == null) {
            $this->_featureIds = ArrayHelper::getColumn($this->features, 'id');
        }
        return $this->_featureIds;
    }

    /**
     * @param $value
     */
    public function setFeatureIds($value)
    {
        $this->_featureIds = $value;
    }

    /**
     * @return string categories in html
     */
    public function featuresAsHtml()
    {
        $features = ArrayHelper::map($this->features, 'id', 'name');

        foreach ($features as $id => $name) {
            $features[$id] = Html::tag('label', $name, ['class' => 'label label-default']);
        }

        return implode(' ', $features);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $oldFeatures = ArrayHelper::getColumn($this->features, 'id');

        $featuresToDelete = array_diff($oldFeatures, $this->featureIds);
        $featuresToCreate = array_diff($this->featureIds, $oldFeatures);

        if (!empty($featuresToDelete)) {
            RoomFeatureItem::deleteAll(['id' => $featuresToDelete]);
        }

        if (!empty($featuresToCreate)) {
            self::getDb()
                ->createCommand()
                ->batchInsert(
                    RoomFeatureItem::tableName(),
                    ['room_id', 'feature_id'],
                    array_map(function ($featureId) {
                        return [$this->id, $featureId];
                    }, $featuresToCreate)
                )
                ->execute();
        }
    }
}
