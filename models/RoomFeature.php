<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%room_feature}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property RoomFeatureItem[] $roomFeatureItems
 */
class RoomFeature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%room_feature}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomFeatureItems()
    {
        return $this->hasMany(RoomFeatureItem::className(), ['feature_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['id' => 'room_id'])->via('roomFeatureItems');
    }

    /**
     * @return array
     */
    public static function getCheckboxList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
